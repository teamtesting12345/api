<?php
/*********************************************************
 Author: 		Manish Giri
 Copyright: 	        Mobiotics IT Solution Private Limited
 Version: 		1.0
 Date:			22-March-2016
 FileName: 		index.php
 Description:           Student API
 **********************************************************/

header("Content-Type:application/json");
error_reporting(E_ALL);

require_once('utilities/helpers.php');
//require_once('utilities/mysqlutilities.php');


function rest_get($request)
{
	$parts = parse_url($request);
	$path_parts = pathinfo($parts['path']);
	$id = $path_parts['filename'];

	error_log("GET_Data===>".json_encode($_GET));
	
	
	

	$result = listData($_GET);
	
	error_log("GET_Result===>".json_encode($result));

	print json_encode($result);
}

function rest_post($request)
{
	$parts = parse_url($request);
	$path_parts = pathinfo($parts['path']);
	$id = $path_parts['filename'];

	error_log("POST_Data===>".json_encode($_POST));

	$result = insertDataWithAutoIncrement($_POST);
	
	error_log("POST_Result===>".json_encode($result));

	print json_encode($result);
}

function rest_put($request)
{
	$parts = parse_url($request);
	$path_parts = pathinfo($parts['path']);
	$id = $path_parts['filename'];

	$put_vars = json_decode(file_get_contents('php://input'),true);

	error_log("PUT_Data===>".json_encode($put_vars));

	$result = updateData($put_vars);
	
	error_log("PUT_Result===>".json_encode($result));

	print json_encode($result);
}

function rest_delete($request)
{
	$parts = parse_url($request);
	$path_parts = pathinfo($parts['path']);
	$id = $path_parts['filename'];

	$put_vars = json_decode(file_get_contents('php://input'),true);

	error_log("DELETE_Data===>".json_encode($put_vars));

	$result = deleteData($put_vars);

	error_log("DELETE_Result===>".json_encode($result));

	print json_encode($result);
}

function rest_error($request)
{
	$code = 405;
	$text = "HTTP Method Not Allowed";
	$errorData = 'HTTP/1.1'.' '.$code.' '.$text;
	header($errorData);
	exit($message);
}

//First check what is the method
if(!isset($_SERVER['REQUEST_METHOD']) || !isset($_SERVER['REQUEST_URI']))
{
	$code = 400;
	$text = "HTTP Method or request URI is not set";
	$errorData = 'HTTP/1.1'.' '.$code.' '.$text;
	header($errorData);
	exit($message);
}

$method = $_SERVER['REQUEST_METHOD'];
$request = $_SERVER['REQUEST_URI'];

switch($method)
{
	case 'POST':
	rest_post($request);
	break;

	case 'GET':
	rest_get($request);
	break;

	case 'PUT':
	rest_put($request);
	break;

	case 'DELETE':
	rest_delete($request);
	break;

	default:
	rest_error($request);
	break;
}

exit(0);
?>



