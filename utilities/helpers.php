<?php

define("VLIVE_DB_USERNAME", "schooluser");
define("VLIVE_DB_PASSWORD", "schoolpass");
define("VLIVE_DB_SERVER", "localhost");
define("VLIVE_DB_Name", "student");
define("VLIVE_DB_PAGELIMIT", "5");
function getMySQLConnection()
{
    $mysqlcon = new mysqli(VLIVE_DB_SERVER,VLIVE_DB_USERNAME,VLIVE_DB_PASSWORD,VLIVE_DB_Name);
    
    if($mysqlcon->connect_errno)
    {
        $errorNo = $mysqlcon->connect_errno;
        $errorData = $mysqlcon->connect_error;        
        error_log("Failed to connect to MySQL: (" . $errorNo . ") " . $errorData);
        exit($errorData);
    }
    
    if(!$mysqlcon->set_charset('utf8'))
    {
        $errorData = $mysqlcon->error;
        error_log("Error loading character set utf8: " . $errorData);
        exit($errorData);
    }
    
    return $mysqlcon;
}

function listData($data)
{
    $mysqlcon = getMySQLConnection();
	$page=getQueryWithPagination($data);
	
	$query = "SELECT * FROM stu_details $page";
	error_log($query);
    
    if(!($result = $mysqlcon->query($query)))
    {
        error_log("listData_Failed: (" . $mysqlcon->errno . ") " . $mysqlcon->error);
        $mysqlcon->close();
        return false;
    }
    
    if ($result->num_rows == 0)
    {
        error_log("listData : No Rows Found");
        $mysqlcon->close();
        return false;
    }
    
    $data = array();
	$data_arr["records"]=array();
    
    while($row = $result->fetch_assoc())
    {
		 extract($row);
		 $data=array(
            "id" => $id,
            "name" => $name,
            "class" => $class,
            "mark" => $mark
        );
 
        array_push($data_arr["records"], $data);
    }    
    
    $result->close();
    $mysqlcon->close();
    
    return $data_arr;
}

function insertDataWithAutoIncrement($data)
{
    $mysqlcon = getMySQLConnection();
    
    $keys = array();
    $values = array();
    
    foreach($data as $key=>$value)
    {
        $keys[] = mysql_fix_string($key,$mysqlcon);
        $values[] = mysql_fix_string($value,$mysqlcon);
    }
    
    //$query = "INSERT INTO bill (".implode(",",$keys).",created) VALUES ('".implode("','",$values)."',NOW())";
    
    $query = "INSERT INTO stu_details (".implode(",",$keys).") VALUES ('".implode("','",$values)."')";
    
    error_log($query);
    
    if(!($stmt = $mysqlcon->prepare($query)))
    {
        error_log("insertDataWithAutoIncrement Failed: (" . $mysqlcon->errno . ") " . $mysqlcon->error);
        return $data;
    }
    
    //Execute query
    if(!$stmt->execute())
    {
        error_log("Unable to insert data: (" . $stmt->errno . ") " . $stmt->error);
        $mysqlcon->close();
        return $data;
    }
    
    if($stmt->affected_rows==0)
    {
        error_log("Duplicate Entry");
        $mysqlcon->close();
        return false;
    }
    
    $newid = $mysqlcon->insert_id;
    $mysqlcon->close();
    
    return $newid." id is Created";
}

function updateData($data)
{
    $mysqlcon = getMySQLConnection();
    
	$id = $data["id"];
  
	
	if(!$id)
    {
        error_log("No id VALUES");
        
        return "No id VALUES";
    }
	
	$keys = array();
    $values = array();
    
    foreach($data as $key=>$value)
    {
       
		$updatevalue[]=mysql_fix_string($key,$mysqlcon). "='" .mysql_fix_string($value,$mysqlcon)."'";
    }
	
	$query = "UPDATE `stu_details` SET ".implode( ',',$updatevalue)." WHERE id =$id";
    error_log($query);
    
    if(!($stmt = $mysqlcon->prepare($query)))
    {
        error_log("updateData Failed: (" . $mysqlcon->errno . ") " . $mysqlcon->error);
        return "updateData Failed";
    }
    
    //Execute query
    if(!$stmt->execute())
    {
        error_log("Unable update data: (" . $stmt->errno . ") " . $stmt->error);
        $mysqlcon->close();
        return "Unable update data:";
    }
    
    if($stmt->affected_rows==0)
    {
        error_log("Duplicate Entry");
        $mysqlcon->close();
        return "Duplicate Entry";
    }
	

    $mysqlcon->close();
    return "Data Is Updated";
}

function deleteData($data)
{
    $mysqlcon = getMySQLConnection();
    
	$id = $data["id"];
   
	
	if(!$id)
    {
        error_log("No id VALUES");
        
        return "No id VALUES";
    }
    
	$query = "DELETE FROM `stu_details` WHERE id =$id";
    error_log($query);
    
    if(!($stmt = $mysqlcon->prepare($query)))
    {
        error_log("Data Failed: (" . $mysqlcon->errno . ") " . $mysqlcon->error);
        return "Data delete Failed";
    }
    
    //Execute query
    if(!$stmt->execute())
    {
        error_log("Unable delete data: (" . $stmt->errno . ") " . $stmt->error);
        $mysqlcon->close();
        return "Unable delete data:";
    }
    
    if($stmt->affected_rows==0)
    {
        error_log("invalid id");
        $mysqlcon->close();
        return "invalid id";
    }
	
    
    $mysqlcon->close();
    return "Data Is deleted";
}
function mysql_fix_string($string,$mysqlcon)
{
	$string = strip_tags($string);

	$string = trim($string);

	$string = preg_replace('/[^[:print:]]/', '',$string);

	if(get_magic_quotes_gpc())
	{
		$string = stripslashes($string);
	}

	$string = $mysqlcon->real_escape_string($string);

	return $string;
}

function getTotalCount($query,$mysqlcon)
{
	//hlog_error_log("getTotalCount===>".$query);

	$totalcount = 0;

	if($result = $mysqlcon->query($query))
	{
		$totalcount = $result->num_rows;
		$result->close();
		//hlog_error_log("getTotalCount===>".$totalcount);
	}

	return $totalcount;
}

function getQueryWithPagination($params)
{
    $page = 1;
    $pagesize = VLIVE_DB_PAGELIMIT;
    
    if(!empty($params['page']))
    {
        $page = $params['page'];
    }
    
    if(!empty($params['pagesize']))
    {
        $pagesize = $params['pagesize'];
    }
    
    $thispage = ($page-1) * $pagesize;
    
    $result = sprintf(" LIMIT %s,%s",$thispage,$pagesize);
    
    return $result;
}

?>
